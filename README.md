# Site de démonstration Drupal

Gros bac à sable pour tester des trucs et développer des modules.

# Urls
Prod : https://drupal.kgaut.xyz
Pré-prod : https://preprod.drupal.kgaut.xyz
Local (via docker compose) : http://drupal.kgaut.test:8000
