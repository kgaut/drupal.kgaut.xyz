# drupal.kgaut.xyz - Changelog

## 1.1.1 - 02/09/2024
- feat(qa): create test module for QA testing

## 1.1.1 - 02/09/2024
- feat(qa): setup reporting

## 1.1.0 - 02/09/2024
- feat(composer): D11 Upgrade
- feat(archi): refactoring

## 1.0.10 - 16/07/2024
- feat(qa): add phpstan and phpcs
- feat(composer): Installing drupal/stage_file_proxy (2.1.5)
- feat(module): enable devel in dev
- feat(node): remove comments
- feat(keepeek): add patchs

## 1.0.9 - 16/07/2024
- feat(readme): update
- feat(keepeek_migrator): scaffolding

## 1.0.8 - 16/07/2024
- feat(archi): add gitignore file to files/tmp and files/privates

## 1.0.7 - 16/07/2024
- feat(composer): Installing drupal/module_filter (5.0.1)
- feat(archi): add gitignore file to config/prod

## 1.0.6 - 16/07/2024
- feat(script): test post / pre-deploy
- feat(ci): bump CI_TEMPLATE_VERSION to main
- feat(composer): update packages

## 1.0.5 - 16/07/2024
- feat(ckeditor): setup media integration

## 1.0.4 - 06/06/2024
- feat(ckeditor): setup media integration

## 1.0.3 - 06/06/2024
- feat(keepeek): update default settings with dummy values

## 1.0.2 - 06/06/2024
- feat(composer): Upgrading drupal/core (10.2.4 => 10.2.7)
- feat(setting): update temporary folder
- feat(setting): setup keepeek

## 1.0.1 - 17/04/2024
- feat(ci): bump CI_TEMPLATE_VERSION to 0.4.6
- feat(archi): change PREPROD_BRANCH to main

## 1.0.0 - 27/03/2024
- feat(archi): initial drupal installation
- feat(CI): Setup
- feat(archi): export config
- feat(CI): disable assets job
