<?php

$key = 'migration.post-deploy.1';


echo "---------------------" . PHP_EOL;
echo "Test execution upgrade" . PHP_EOL;
echo "state : $key" . PHP_EOL;
echo "Valeure actuelle : " . (int) \Drupal::state()->get($key) . PHP_EOL;


if(!\Drupal::state()->get($key)) {
  echo "Script $key non joué" . PHP_EOL;
  echo "Execution script $key" . PHP_EOL;
  \Drupal::state()->set($key, TRUE);
}
else {
  echo "Script $key déjà joué" . PHP_EOL;
  echo "Rien à faire" . PHP_EOL;
}

echo "---------------------" . PHP_EOL;
